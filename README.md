# UNTree

#### 介绍
垂直树形控件,采用原生js编写,经webpack 打包处理,带sourcemap 31kb, 不带sourcemap仅12kb, 可用作部门组织架构树, 任务分发树展示等等, 可以传入 json数组 或 已经转化后的json树

#### 预览

[https://zhuzhaofeng.gitee.io/untree/](https://zhuzhaofeng.gitee.io/untree/)

#### 使用

> 引入`dist/un-tree.min.js`

```html
<script src="./un-tree.min-sourcemap.js" type="text/javascript" charset="utf-8"></script>
```

> 初始化树

```javascript
let tree = new UNTree(options);
tree.render();
```

**options 解释**

| 参数          | 说明                                                  | 默认值 | 可选值       |
| ------------- | ----------------------------------------------------- | ------ | ------------ |
| el            | 加载元素                                              |        |              |
| jsonArr       | 数据                                                  | []     |              |
| selfIdField   | 每条数据的主键字段                                    | id     |              |
| parentIdField | 每条数据的父级字段                                    | parent |              |
| parentld      | 第一级数据的父级id                                    | ""     |              |
| text          | 需要显示的字段                                        | text   |              |
| type          | 数据类型                                              | list   | list \| tree |
| viewClass     | 自定义整体视图class                                   |        |              |
| itemClass     | 自定义单独项class                                     |        |              |
| click         | 单击事件(返回三个参数itemId, itemData, itemNode) |        |              |

#### 示例

- 加载json数组

  ```html
  <div id="un-tree--wrapper"></div>
  ```

  

  ```javascript
  let listData = [
    {
      id: "99d78438-1f14-4552-ba77-039bff75e4bc",
      text: "公司董事会",
      parent: "root",
    },
    {
      id: "d2eb4680-4cee-4549-8a0f-171fd9888f4b",
      text: "总经理",
      parent: "99d78438-1f14-4552-ba77-039bff75e4bc",
    },
    {
      id: "18c9f830-e699-4fe1-b79c-71a6b34b7e8f",
      text: "副总经理1",
      parent: "d2eb4680-4cee-4549-8a0f-171fd9888f4b",
    },
    {
      id: "b05991b4-8944-4dd8-92c4-ffd7848e7112",
      text: "副总经理2",
      parent: "d2eb4680-4cee-4549-8a0f-171fd9888f4b",
    },
    {
      id: "f135c05b-ffde-4d82-895f-a6daf052c178",
      text: "副总经理3",
      parent: "d2eb4680-4cee-4549-8a0f-171fd9888f4b",
    },
    {
      id: "234e9d2e-676b-4601-9820-e9b064761ff4",
      text: "技术总监",
      parent: "d2eb4680-4cee-4549-8a0f-171fd9888f4b",
    },
    {
      id: "7937421a-ee31-4988-934d-7e6469f0f37f",
      text: "数据中心",
      parent: "18c9f830-e699-4fe1-b79c-71a6b34b7e8f",
    },
    {
      id: "11c1b052-89f8-4873-a876-6de6130385e5",
      text: "测量中心",
      parent: "18c9f830-e699-4fe1-b79c-71a6b34b7e8f",
    },
    {
      id: "e9c44ca7-5302-4ae7-836a-2adc93e0f5d4",
      text: "财务部",
      parent: "b05991b4-8944-4dd8-92c4-ffd7848e7112",
    },
    {
      id: "e9c44ca7-5302-4ae7-836a-2adc93e0f5d4",
      text: "办公室",
      parent: "b05991b4-8944-4dd8-92c4-ffd7848e7112",
    },
    {
      id: "bc407b45-56ef-4bdc-ab31-325d4cad5243",
      text: "项目部",
      parent: "f135c05b-ffde-4d82-895f-a6daf052c178",
    },
    {
      id: "71fc1ee3-5f31-49a8-8cdc-086c92bff785",
      text: "评估一部",
      parent: "f135c05b-ffde-4d82-895f-a6daf052c178",
    },
    {
      id: "9a0214ba-4afc-43ce-9851-fbc79dd48f25",
      text: "技术部",
      parent: "234e9d2e-676b-4601-9820-e9b064761ff4",
    },
    {
      id: "71fc1ee3-5f31-49a8-8cdc-086c92bff785",
      text: "评估二部",
      parent: "234e9d2e-676b-4601-9820-e9b064761ff4",
    },
  ];
  
  function handleClick(itemId, itemData, itemNode) {
    console.log(itemId);
    console.log(itemData);
    console.log(itemNode);
  }
  let el = document.getElementById("un-tree--wrapper");
  let tree = new UNTree({
    el: el,
    jsonArr: listData,
    parentld: "root",
    type: "list",
    viewClass: "view-demo",
    itemClass: "item-demo",
    click: handleClick,
  });
  tree.render();
  ```

- 加载转化后的json树

  ```html
  <div id="un-tree--wrapper2"></div>
  ```

  

  ```javascript
  let treeData = [
    {
      id: "99d78438-1f14-4552-ba77-039bff75e4bc",
      text: "公司董事会",
      parent: "root",
      children: [
        {
          id: "d2eb4680-4cee-4549-8a0f-171fd9888f4b",
          text: "总经理",
          parent: "99d78438-1f14-4552-ba77-039bff75e4bc",
          children: [
            {
              id: "18c9f830-e699-4fe1-b79c-71a6b34b7e8f",
              text: "副总经理1",
              parent: "d2eb4680-4cee-4549-8a0f-171fd9888f4b",
              children: [
                {
                  id:
                  "7937421a-ee31-4988-934d-7e6469f0f37f",
                  text: "数据中心",
                  parent:
                  "18c9f830-e699-4fe1-b79c-71a6b34b7e8f",
                },
                {
                  id:
                  "11c1b052-89f8-4873-a876-6de6130385e5",
                  text: "测量中心",
                  parent:
                  "18c9f830-e699-4fe1-b79c-71a6b34b7e8f",
                },
              ],
            },
            {
              id: "b05991b4-8944-4dd8-92c4-ffd7848e7112",
              text: "副总经理2",
              parent: "d2eb4680-4cee-4549-8a0f-171fd9888f4b",
              children: [
                {
                  id:
                  "e9c44ca7-5302-4ae7-836a-2adc93e0f5d4",
                  text: "财务部",
                  parent:
                  "b05991b4-8944-4dd8-92c4-ffd7848e7112",
                },
                {
                  id:
                  "e9c44ca7-5302-4ae7-836a-2adc93e0f5d4",
                  text: "办公室",
                  parent:
                  "b05991b4-8944-4dd8-92c4-ffd7848e7112",
                },
              ],
            },
            {
              id: "f135c05b-ffde-4d82-895f-a6daf052c178",
              text: "副总经理3",
              parent: "d2eb4680-4cee-4549-8a0f-171fd9888f4b",
              children: [
                {
                  id:
                  "bc407b45-56ef-4bdc-ab31-325d4cad5243",
                  text: "项目部",
                  parent:
                  "f135c05b-ffde-4d82-895f-a6daf052c178",
                },
                {
                  id:
                  "71fc1ee3-5f31-49a8-8cdc-086c92bff785",
                  text: "评估一部",
                  parent:
                  "f135c05b-ffde-4d82-895f-a6daf052c178",
                },
              ],
            },
            {
              id: "234e9d2e-676b-4601-9820-e9b064761ff4",
              text: "技术总监",
              parent: "d2eb4680-4cee-4549-8a0f-171fd9888f4b",
              children: [
                {
                  id:
                  "9a0214ba-4afc-43ce-9851-fbc79dd48f25",
                  text: "技术部",
                  parent:
                  "234e9d2e-676b-4601-9820-e9b064761ff4",
                },
                {
                  id:
                  "71fc1ee3-5f31-49a8-8cdc-086c92bff785",
                  text: "评估二部",
                  parent:
                  "234e9d2e-676b-4601-9820-e9b064761ff4",
                },
              ],
            },
          ],
        },
      ],
    },
  ];
  function handleClick(itemId, itemData, itemNode) {
    console.log(itemId);
    console.log(itemData);
    console.log(itemNode);
  }
  let el2 = document.getElementById("un-tree--wrapper2");
  let tree2 = new UNTree({
    el: el2,
    jsonArr: treeData,
    parentld: "root",
    type: "tree",
    viewClass: "view-demo",
    itemClass: "item-demo",
    click: handleClick,
  });
  tree2.render();
  ```

  