"use strict";
/**
 * @author zhuzhaofeng
 * 常用工具类
 * @dateTime 2019年9月11日10:48:49
 */
class CommonUtils {
    constructor() {}
    /**
     * 是否是数组
     * @param val 需要判断的值
     * @returns {boolean}
     */
    isArray(val) {
        return toString.call(val) === "[object Array]";
    }
    /**
     * 是否是字符串
     * @param val
     * @returns {boolean}
     */
    isString(val) {
        return typeof val === "string";
    }
    /**
     * 去除字符串左右两边空格
     * @param {String} str 需要去除空格的字符串
     */
    trim(str = "") {
        return str.replace(/^\s*|\s*$/g, "");
    }
    /**
     * 递归遍历json 生成树形
     * @param jsonArr 需要遍历的json数组
     * @param selfIdField 每条数据id字段
     * @param parentIdField 每条数据父级id字段
     * @param parentld 最顶级 父级id值
     * @returns {[]|Array}
     */
    convertToTreeData(
        jsonArr = [],
        selfIdField = "id",
        parentIdField = "parentId",
        parentld = ""
    ) {
        let result = [];
        if (this.isArray(jsonArr) && this.isString(parentIdField)) {
            if (jsonArr.length < 1) {
                return [];
            } else {
                for (const val of jsonArr) {
                    if (val[`${parentIdField}`] === parentld) {
                        let obj = {};
                        for (const key in val) {
                            obj[`${key}`] = val[`${key}`];
                        }
                        let children = this.convertToTreeData(
                            jsonArr,
                            selfIdField,
                            parentIdField,
                            obj[`${selfIdField}`]
                        );
                        if (children.length > 0) {
                            obj.children = children;
                        } else{
                            obj.children = []
                        }
                        result.push(obj);
                    }
                }
            }
        } else {
            throw "传入参数格式不正确";
        }
        return result;
    }
}
/**导出 */
export default new CommonUtils();
