import './styles/base.scss'
import Tree from './Tree';

if (typeof module !== "undefined" && module.exports) {
	module.exports = CommonUtils;
} else if (typeof define === "function" && define.amd) {
	define(function(){return Tree;});
} else {
	!('CommonUtils' in window) && (window.UNTree = Tree);
}
