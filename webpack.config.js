const path = require("path");
const webpack = require("webpack")
const HtmlWebpackPlugin = require("html-webpack-plugin");
module.exports = {
    entry: "./src/index.js",
    // devtool: 'inline-source-map',
    output: {
        path: __dirname,
        filename: "./dist/un-tree.min.js",
    },
    module: {
        rules: [
            { test: /\.css$/, use: ["style-loader", "css-loader"] },
            {
                test: /\.scss$/,
                use: [
                    { loader: "style-loader" },
                    {
                        loader: "css-loader",
                    },
                    {
                        loader: "postcss-loader",
                        options: {
                            config: {
                                path: "postcss.config.js", // 这个得在项目根目录创建此文件
                            },
                        },
                    },
                    {
                        loader: "sass-loader"
                    },
                ],
            },
            {
                test: /\.js?$/,
                exclude: /(node_modules)/,
                loader: "babel-loader",
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./dist/index.html",
        }),
        new webpack.BannerPlugin('\n @author zhuzhaofeng\n @description 树形图 \n https://gitee.com/zhuzhaofeng/UNTree '),
    ],
    devServer: {
        contentBase: path.join(__dirname, "./dist"),
        port: 8080,
        open: true,
    },
};
